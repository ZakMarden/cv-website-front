document.addEventListener('DOMContentLoaded', function() {
    document.querySelector(".title").style.opacity = '1';
    const subtitleText = "Cloud Engineer, North Wales";
    const subtitleElement = document.getElementById("role");

    function typeText() {
        let charIndex = 0;

        const typeChar = () => {
            if (charIndex < subtitleText.length) {
                subtitleElement.textContent += subtitleText.charAt(charIndex);
                charIndex++;
                setTimeout(typeChar, 75);
            } else {
                showButtons();
            }
        };

        typeChar();
    }

    setTimeout(typeText, 750);

    const hamburger = document.querySelector('.hamburger');
    const sidenav = document.getElementById('mySidenav');
    const closeBtn = document.querySelector('.closebtn');

    hamburger.addEventListener('click', function() {
        sidenav.style.width = "12em";
    });

    closeBtn.addEventListener('click', function() {
        sidenav.style.width = "0";
    });

    function showButtons() {
        var buttons = document.querySelectorAll('.button-container button');
        buttons.forEach(function(button, index) {
            setTimeout(function() {
                button.style.opacity = 1;
            }, index * 250); // Delay each button
        });
    }
});

function scrollToSection(id) {
        document.getElementById(id).scrollIntoView({ behavior: 'smooth' });
    }